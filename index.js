let submitButton = document.querySelector('.btn');
let firstShowPasswordIcon = document.querySelector('.fas.first.fa-eye.icon-password');

firstShowPasswordIcon.addEventListener('click', ()=> {
    let password = document.querySelector('.first-password'); 
    if(password.type === "password"){
        password.type = "text";
        firstShowPasswordIcon.classList.replace('fa-eye', 'fa-eye-slash');
    }
    else {
        password.type = "password";
        firstShowPasswordIcon.classList.replace('fa-eye-slash', 'fa-eye');
    }
})

let secondShowPasswordIcon = document.querySelector('.fas.second.fa-eye.icon-password');
secondShowPasswordIcon.addEventListener('click', ()=> {
    let password = document.querySelector('.second-password'); 
    if(password.type === "password"){
        password.type = "text";
        secondShowPasswordIcon.classList.replace('fa-eye', 'fa-eye-slash');
    }
    else {
        password.type = "password";
        secondShowPasswordIcon.classList.replace('fa-eye-slash', 'fa-eye');
    }
})

submitButton.addEventListener('click', () => {
    let firstInput = document.querySelector('.first-password');
    let secondInput = document.querySelector('.second-password');
    if(firstInput.value === secondInput.value){ 
        alert('YOU ARE WELCOME')}
    else { 
        let errorSpan = document.createElement('span');
        errorSpan.innerText = "Нужно ввести одинаковые значения";
        errorSpan.className = "error-span";
        secondInput.after(errorSpan)
}
})